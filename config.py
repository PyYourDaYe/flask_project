import logging
from redis import StrictRedis

class Config(object):
    SECRET_KEY = 'JLJLJLLJLJLJFDFDSFSDFDSF'
    # 数据库配置
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/flasktest'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # redis配置
    REDIS_HOST = '127.0.0.1'
    REDIS_POST = 6379
    # flask_session的配置信息
    SESSION_TYPE = 'redis' # 指定 session 保存到redis中
    SESSION_USE_SIGNER = True # 让 cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_POST) # 使用redis的实例
    PERMANENT_SESSION_LIFETIME = 86400  #设置session过期时间

    LOG_LEVEL = logging.DEBUG

class DevelopmentConfig(Config):
    #开发环境下的配置
    DEBUG = True

class ProductionConfig(Config):
    #生产环境下的配置
    DEBUG = False
    LOG_LEVEL = logging.ERROR

class TestingConfig(Config):
    #单元测试环境下的配置
    DEBUG = True
    TESTING = True

#定义配置字典
config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
