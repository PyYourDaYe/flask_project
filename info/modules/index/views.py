import logging
from flask import current_app
from . import index_blu

@index_blu.route('/')
def index():
    logging.debug("This is a debug log.")
    logging.info("This is a info log.")
    logging.warning("This is a warning log.")
    logging.error("This is a error log.")
    logging.critical("This is a critical log.")
    current_app.logger.debug('debug')
    current_app.logger.error('error')
    return "反对封建等级分的积分"